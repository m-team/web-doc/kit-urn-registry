[![kit-logo](files/kit.jpg)](https://kit.edu)

<a href="https://www.kit.edu" title="KIT-Logo"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 196.18 90.32" width="196.18px" height="90.32px" x="0px" y="0px" role="img" aria-labelledby="kit-logo-alt-title">

# KIT URN Registry

This registry handles entries for 

<!--- [urn:geant:helmholtz.de](helmholtz.md)-->
<!---->
<!--- [urn:geant:h-df.de](hdf.md)-->

- [urn:geant:kit.edu](kit.md)


