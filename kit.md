# urn:geant:kit.edu Registry

## Registered Names:

- `urn:geant:kit.edu:res` contains resource claims according to [AARC-G027](https://aarc-community.org/guidelines/aarc-g027)
 

| URN  (PREFIX: `urn:geant:kit.edu:res:`) | Purpose                                                                                         | Last Changed | Contact       |
|---------------------------------------|-------------------------------------------------------------------------------------------------|--------------|---------------|
| `fels:idp-admin`              | Identify Home-Idp Admins that are authorised to see Users of their home-org on fels.scc.kit.edu | 05/2021      | Michael Simon |
